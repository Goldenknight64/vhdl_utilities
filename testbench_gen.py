import sys
from os import path
from shutil import copyfile
import vhdl

def write_testbench(filename, entity_name, includes, generics, ports):
	write_string = '-- Automatically generated using the testbench_gen utility.\n\n'
	
	#--Preamble--------------------------------
	for line in includes:
		write_string = write_string + line + '\n'
	#------------------------------------------
	
	#--Entity----------------------------------
	write_string = write_string + '\nentity {0}_testbench is\nend {0}_testbench;\n\n'.format(entity_name)
	#------------------------------------------

	#--Architecture----------------------------
	write_string = write_string + 'architecture behavioral of {0}_testbench is\n'.format(entity_name)
	#------------------------------------------

	#--Component-------------------------------
	write_string = write_string + '\tcomponent {0}\n'.format(entity_name)
	if len(generics) > 0:
		write_string = write_string + '\t\tgeneric(\n'
		for generic in generics:
			if 'init' in generic:
				init_format = ' := ' + generic['init']
			else:
				init_format = ''

			write_string = write_string + '\t\t\t{0}: {1};\n'.format(generic['name'], generic['type'], init_format)
		write_string = write_string[:-2]
		write_string = write_string + '\n\t\t);\n'
		
	if len(ports) > 0:
		write_string = write_string + '\t\tport(\n'
		for port in ports:
			if 'init' in ports:
				init_format = ' := ' + port['init']
			else:
				init_format = ''
			write_string = write_string + '\t\t\t{0}: {1} {2}{3};\n'.format(port['name'], port['direction'], port['type'], init_format)
		write_string = write_string[:-2]
		write_string = write_string + '\n\t\t);\n'
	write_string = write_string + '\tend component;\n\n'
	#------------------------------------------

	#--Signals---------------------------------
	for port in ports:
		# Special override for the clk init value.
		if port['name'] == 'clk' and ('init' not in port or port['init'] == ''):
			port['init'] = "'1'"
		
		if 'init' in port and port['init'] != '':
			init_format = ' := {0}'.format(port['init'])
		else:
			init_format = ''
			
		write_string = write_string + '\tsignal {0}: {1}{2};\n'.format(port['name'], port['type'], init_format)

	write_string = write_string + 'begin\n'
	#------------------------------------------

	#--Init component--------------------------
	write_string = write_string + '\tUUT: {0}\n'.format(entity_name)
	if len(generics) > 0:
		write_string = write_string + '\t\tgeneric map(\n'
		for generic in generics:
			write_string = write_string + '\t\t\t{0} => {0},\n'.format(generic['name'])

		write_string = write_string[:-2]
		if len(ports) == 0:
			gen_semicolon=';'
		else:
			gen_semicolon=''
		write_string = write_string + '\n\t\t){0}\n'.format(gen_semicolon)
		
	if len(ports) > 0:
		write_string = write_string + '\t\tport map(\n'
		for port in ports:
			if 'init' in ports:
				init_format = ' := {0}' + port['init']
			else:
				init_format = ''
			write_string = write_string + '\t\t\t{0} => {0},\n'.format(port['name'], port['direction'], port['type'], init_format)
		write_string = write_string[:-2]
		write_string = write_string + '\n\t\t);\n\n'
	#------------------------------------------

	for port in ports:
		if port['name'] == 'clk':
			write_string = write_string + '\tclk <= not clk after 5 ns;\n'

	#--user defined ---------------------------
	write_string = write_string + '\n\tprocess\n\tbegin\n\t\t-- User code here.\n'
	for port in ports:
		if port['direction'] == 'in' and port['name'] != 'clk':
			write_string = write_string + '\t\t{0} <= ;\n'.format(port['name'])
	write_string = write_string + '\t\twait for 10 ns;\n\n\t\twait;\n\tend process;\nend behavioral;\n'
	#------------------------------------------
	
	with open(filename, 'w') as file:
		file.write(write_string)
	
def main():
	if len(sys.argv) < 2:
		print('error: filename not specified.')
		sys.exit(-1)
	
	input_filename = sys.argv[1]
	
	if not path.exists(input_filename):
		print('error: file not found.')
		exit(-2)

	info = vhdl.parse_vhdl(input_filename)
	
	output_filename = '{0}_testbench.vhd'.format(info['entity_name'])
	output_dir = path.dirname(path.abspath(input_filename))
	output_file = path.join(output_dir,output_filename)

	# Backup an existing testbench.
	if path.exists(output_file):
		backup_file = output_file + '.bak'

		i = 0
		# Get a free name for the backup file.
		while path.exists(backup_file):
			i = i + 1
			backup_file = output_file + '.bak' + str(i)

		copyfile(output_file, backup_file)
		print('Current Testbench backed up as {0}'.format(backup_file))
	
	write_testbench(output_file, info['entity_name'], info['includes'], info['generics'], info['ports'])
	print('Testbench file generated as {0}'.format(output_file))
	
if __name__ == '__main__':
	main()