@echo off
::--------------------------------------------------
:: Build Script Metadata
set "BUILD_SCRIPT_AUTHOR=Tyler Borrelli"
set "BUILD_SCRIPT_NAME=Build Script
set BUILD_SCRIPT_VERSION=2.0
set BUILD_SCRIPT_LAST_MODIFIED=2/12/19
set "BUILD_SCRIPT_DIR=%~dp0"
set "BUILD_SCRIPT_FILE=%~nx0
::--------------------------------------------------
:: Error Codes
set ERROR_SUCCESS=0
set ERROR_SCRIPT_NOT_FOUND=3
set ERROR_NO_PYINSTALLER=2
set ERROR_NO_PIP=3

::--------------------------------------------------
:: Defualts
set DEFAULT_SCRIPT=testbench_gen.py
set DEFAULT_BIN_DIR=.\bin\testbench_gen
::--------------------------------------------------

set result=%ERROR_SUCCESS 

set "script=%DEFAULT_SCRIPT%"
set "bin=%DEFAULT_BIN_DIR%"

:: Evalue args.
if [%1] == [] (
	goto build
)

if [%1] == [-h] (
	goto helpMenu
) else if [%1] == [--help] (
	goto helpMenu
) else if [%1] == [-v] (
	goto version
) else if [%1] == [--version] (
	goto version
) else (
	set "script=%1"

	if NOT [%2] == [] (
		set "bin=%2"
	)
	goto build
)

goto end
:helpMenu
	echo %BUILD_SCRIPT_NAME% v%BUILD_SCRIPT_VERSION% Last Modified: %BUILD_SCRIPT_LAST_MODIFIED%
	echo   Usage: %BUILD_SCRIPT_FILE% [-h] [-v] [script [bin]]
	echo.
	echo       bin: The directory to write the build files to.
	echo       script: The python script to compile to an exectuable.
	echo.
	echo       -h --help: Show this help menu.
	echo       -v --version: Show the version information.
	echo.
	goto end

:version
	echo %BUILD_SCRIPT_VERSION% Last Modified: %BUILD_SCRIPT_LAST_MODIFIED%
	goto end

:InstallPyinstaller
	:: Check for pip.
	where /Q pip
	if [%errorlevel%] == [1] (
		echo Pip not found.
		exit /b %ERROR_NO_PIP%
	)
	
	:InstallPyinstallerPrompt
	set install=-1
	set /P "resp=Install Pyinstaller (Y/n): "

	:: Default to install.
	if [%resp%] == [] (set install=1)
	for /F "delims=," %%i in ("n,no") do if /I [%resp%] == [%%i] (set install=0)
	for /F "delims=," %%i in ("y,yes") do if /I [%resp%] == [%%i] (set install=1)

	::invalid Response.
	if [%install%] == [-1] (
		echo Invalid response
		goto :InstallPyinstallerPrompt
	)
	
	:: Do not install Pyinstaller.
	if [%install%] == [0] (
		echo Pyinstaller not installed.
		exit /b %ERROR_NO_PYINSTALLER%
	)
	
	:: Install Pyinstaller.
	if [%install%] == [1] (
		pip install pyinstaller
	)
	
	exit /b %ERROR_SUCCESS%
	
:build
	:: Check for Pyinstaller python package.
	where /Q pyinstaller
	if [%errorlevel%] == [1] (
		call :InstallPyinstaller
	)
	
	if [%errorlevel%] == [%ERROR_NO_PIP%] (
		set result = %ERROR_NO_PIP%
		goto end
	)
	
	if [%errorlevel%] == [%ERROR_NO_PYINSTALLER%] (
		set result=%ERROR_NO_PYINSTALLER%
		goto end
	)
	
	:: Build executable
	pyinstaller --dist %bin% --workpath %bin%\build --specpath %bin% --onefile -y %script%
	goto end

:end
popd
exit /b %result%
