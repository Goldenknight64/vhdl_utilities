import sys
from os import path
import vhdl

def print_storage(to_print):
	if type(to_print) is list:
		for el in to_print:
			print(el)

	if type(to_print) is dict:  
		for key in to_print:
			print(key, to_print[key])
			
def get_waves(filename, parents=None):
	waves = []
	ent = vhdl.parse_vhdl(filename)
	
	if parents is None:
		parents = [ent['entity_name']]
	
	for elem in ent['ports']:
		waves.append({
				'parents': parents,
				'name': elem['name']
			}
		)
	
	for elem in ent['signals']:
		waves.append({
				'parents': parents,
				'name': elem
			}
		)
	
	for comp in ent['components']:
		p = parents + [comp['name']]
		filepath = path.dirname(filename)
		f = path.join(filepath,comp['type'] + '.vhd')
		
		if not path.exists(f):
			print('Cannot locate file [{0}].'.format(f))
		else:
			(ent_name, res) = get_waves(f,p)
			waves = waves + res
	
	return (ent['entity_name'], waves)

def write_wave_file(filename, waves):
	sorted_waves = {}
	
	for wave in waves:
		key = '/' + '/'.join(wave['parents'])
		# Create an entry for this parent if it does not exist. Add wave to parent entry.
		if key not in sorted_waves:
			sorted_waves[key] = []
			sorted_waves[key].append(wave['name'])
		else:
			sorted_waves[key].append(wave['name'])
	
	write_string = 'onerror {resume}\nquietly WaveActivateNextPane {} 0\n'
	for key in sorted_waves:
		divider = '/'.join(key.split('/')[2:])
		write_string = write_string + ('add wave -noupdate -divider {{{0}}}\n'.format(divider))
		for signal in sorted_waves[key]:
			line = 'add wave -noupdate -label {0} {1}\n'.format(signal, key + '/' + signal)
			write_string = write_string + line
	
	# Command to update the tree in the waveworm window.
	write_string = write_string + 'TreeUpdate [SetDefaultTree]\n'
	
	with open(filename, 'w') as file:
		file.write(write_string)
	
def main():
	if len(sys.argv) < 2:
		print('error: filename not specified.')
		sys.exit(-1)

	input_filename = sys.argv[1]
	
	if not path.exists(input_filename):
		print('error: file not found.')
		exit(-2)

	(entity_name, waves) = get_waves(input_filename)
	
	output_dir = path.dirname(path.abspath(input_filename))
	output_filename = '{0}_wave.do'.format(entity_name)
	output_path = path.join(output_dir,output_filename)

	write_wave_file(output_path, waves)
	
	print('Wave form file generated as {0}'.format(output_path))

	
if __name__ == '__main__':
	main()