'''
Strip the comments from the vhdl data.
return the filtered data.
'''
def __strip_comments__(data):
	result = data
	
	while True:
		comment_start = result.find('--')
		comment_end = result.find('\n', comment_start)
		
		# Remove the comment region.
		# NOTE: Preserve newline character.
		if comment_start != -1:
			result = result[:comment_start] + result[comment_end:]
		else:
			break

	return result

'''
extract the comments from the vhdl data.
returns a list containing the extracted comments.
'''
def __extract_comments__(data):
	result = []
	
	while True:
		comment_start = data.find('--')
		comment_end = data.find('\n', comment_start)
		
		# Remove the comment region.
		# NOTE: Preserve newline character.
		if comment_start != -1:
			result.append(data[comment_start:comment_end])
			data = data[:comment_start] + data[comment_end:]
		else:
			break

	return result
	
'''
Split the vhdl specified by the input filename into the corresponding tokens.
returns the resulting tokens.
'''
def __tokenize__(data):
	input = __strip_comments__(data)
	
	split_chars = [',', ':', ';','(', ')']
	whitespace_chars = [' ', '\t', '\n', '\r']
	tokens = []
	
	start = 0
	for i in range(0,len(input)):
		for split_char in split_chars:
			if input[i] == split_char:
				if start != i:
					tokens.append(input[start:i])
				tokens.append(input[i])
				start = i+1
		for whitespace_char in whitespace_chars:
			if input[i] == whitespace_char:
				if start != i:
					tokens.append(input[start:i])
				start = i+1

	return tokens

'''
Extract information from a VHDL file.
Return a dictionary containing the results
	'comments', 'includes', 'entity_name', 'ports', 
	'generics', 'components', 'signals'
'''
def parse_vhdl(filename):
	# Extract the contents of the file.
	data = ''
	with open(filename) as file:
		for line in file.readlines():
			data += line
			
	tokens = __tokenize__(data)
			
	comments = __extract_comments__(data)
	includes = __parse_imports__(tokens)
	(entity_name, generics, ports) = __parse_entity__(tokens)
	(signals, components) = __parse_architecture__(tokens)
	
	vhdl_info = {
		'comments': comments,
		'includes': includes,
		'entity_name': entity_name,
		'ports':  ports,
		'generics': generics,
		'components': components,
		'signals': signals
	}
	
	return vhdl_info
	
'''
Parse the libraries and imports from the provided tokens.
return a list of library and import strings.
'''
def __parse_imports__(tokens):
	import_lines = ['library', 'use']
	
	import_statements = []
	for line in __split_list__(tokens, ';'):
		if len(line) > 0 and line[0] in import_lines:
			import_statement = ' '.join(line) + ';'.lower()
			
			if import_statement not in [s.lower() for s in import_statements]:
				import_statements.append(import_statement)	
	return import_statements

'''
Parse the entity portion from the provided tokens.
Returns:
Outputs:
	enity_name [string] -  the name of the entity
	generics [list][dicts] - list of generics, dict form {'name', 'type', 'init'}
	ports [list][dicts] - list of ports, dict form {'name', 'direction', 'type', 'init'}
'''
def __parse_entity__(tokens):
	# Extract the Entity declaration tokens from all tokens.
	entity_start = [token.lower() for token in tokens].index('entity')
	entity_end = [token.lower() for token in tokens].index('end',entity_start) + 1
	entity_tokens = tokens[entity_start: entity_end]

	entity_name = entity_tokens[1]
	ports = []
	generics = []
	
	# Fix special entity tokens.
	for i in range(0, len(entity_tokens)):
		if entity_tokens[i].lower() == 'downto':
			entity_tokens[i] = ' {0} '.format(entity_tokens[i])
	
	generic_tokens = []
	port_tokens = []
	
	# There is a generic section.
	if 'generic' in [token.lower() for token in entity_tokens]:
		generic_start = [token.lower() for token in entity_tokens].index('generic')
	else:
		generic_start = -1
	
	# There is a port section.
	if 'port' in [token.lower() for token in entity_tokens]:
		port_start = [token.lower() for token in entity_tokens].index('port')
	else:
		port_start = -1
	
	# Set the end of the first section to right before the start of the second section.
	# Set the end of the second section to right before the end of the entity.
	if generic_start < port_start:
		if generic_start > 0:
			generic_tokens = entity_tokens[generic_start + 2: port_start - 2]
		if port_start > 0:
			port_tokens = entity_tokens[port_start + 2: -3]
	else:
		if port_start > 0:
			port_tokens = entity_tokens[port_start + 2: generic_start - 2]
		if generic_start > 0:
			generic_tokens = entity_tokens[generic_start + 2: -3]

	# There is a generic section.
	if len(generic_tokens) > 0:
		gen_lines = __split_list__(generic_tokens, ';')
		
		for line in gen_lines:
			line_sections = __split_list__(line,':')
			
			line_names = __split_list__(line_sections[0], ',')
			line_type = ''.join(line_sections[1])

			# Remove the constant keyword from generic names.
			if 'constant' in line_names[0]:
				line_names[0].remove('constant')
			
			# There is an init value provided.
			if len(line_sections) > 2 and line_sections[2][0] == '=':
				line_init = ''.join(line_sections[2][1:])
			else:
				line_init = ''
			
			for name in line_names:
				generics.append({
					'name': ''.join(name),
					'type': line_type,
					'init': line_init
				})
	
	# There is a port section.
	if len(port_tokens) > 0:
		valid_dirs = ['in', 'out', 'inout', 'buffer', 'linkage', 'bus']
		port_lines = __split_list__(port_tokens, ';')
		
		for line in port_lines:
			# Ignore empty lines.
			if len(line) == 0:
				continue
			line_sections = __split_list__(line,':')
			
			line_names = __split_list__(line_sections[0], ',')
			line_dir = line_sections[1][0]
			
			# Verify that the direction was valid.
			# NOTE: if the direction is invalid, assume that the 'direction' value is not provided and the string belongs to the type.
			if line_dir not in valid_dirs:
				line_dir = ''
				line_type = ''.join(line_sections[1])
			else:
				line_type = ''.join(line_sections[1][1:])
			
			# There is an init value provided.
			if len(line_sections) > 2 and line_sections[2][0] == '=':
				line_init = ''.join(line_sections[2][1:])
			else:
				line_init = ''
			
			for name in line_names:
				ports.append({
					'name': ''.join(name),
					'direction': line_dir,
					'type': line_type,
					'init': line_init
				})
			
	return (entity_name, generics, ports)
	
'''
Parse the architecture portion of a VHDL file.
Returns:
	signal [list][strings] - The name of each signal defined in the architecture.
	components [list][dicts]- Return a list of components defining component name and component type.
'''
def __parse_architecture__(tokens):
	arch_start = [token.lower() for token in tokens].index('architecture')
	arch_name = tokens[arch_start + 1].lower()
	try:
		# Architecture name ends architecture block.
		arch_end = [token.lower() for token in tokens].index(arch_name, arch_start + 2)
	except ValueError as e:
		# Search for architecture keyword to end block.
		try:
			arch_end = [token.lower() for token in tokens].index('architecture', arch_start + 2)
		except ValueError as e:
			# Neither architecture keyword or name ends architecture block, cannot process.
			return None
		
	arch_tokens = tokens[arch_start:arch_end]
	
	signals = []
	component_types = []
	components = []
	
	#print_list(arch_tokens)
	i = 0
	while i < len(arch_tokens):
		cur = arch_tokens[i]
	
		# Extract all signal names.
		if cur.lower() == 'signal':
			for i in range(i+1, len(arch_tokens)):
				cur = arch_tokens[i]
				
				# Signal declaration terminator, stop processing.
				if cur == ':':
					break
				# Signal seperator, ignore.
				elif cur != ',': 
					signals.append(cur)
				
			i = i + 1
		# Add Component type to list.
		elif (cur.lower() == 'component'):
			# The component name.
			component_types.append(arch_tokens[i+1].lower())
			
			# Find the end of the component declaration.
			for i in range(i + 1, len(arch_tokens)):
				if (arch_tokens[i].lower() == 'component'):
					break;

		# Get the name of component definition.
		elif (cur.lower() in component_types):
			if (arch_tokens[i-1] == ':'):
				components.append({
					'name': arch_tokens[i - 2],
					'type': cur
				})

		i = i + 1

	return (signals, components)

'''
Split the input list at the seperator(s).
Output a list containing each seperated segment. 
'''
def __split_list__(list, seperators=','):
	lines = [[]]
	for token in list:
		if token == seperators or token in seperators:
			lines.append([])
		else:
			lines[-1].append(token)
			
	return lines